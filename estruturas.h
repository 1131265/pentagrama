#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

typedef struct Estado {
	GLint       delay;
	GLboolean doubleBuffer;
}Estado;

typedef struct Modelo {
	GLint anim;
	GLdouble raio;
	GLdouble inc;
	GLdouble angulo;
	GLdouble anguloInicial;
}Modelo;