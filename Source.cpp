#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "estruturas.h"
#define RAIO_INI 0.5

Estado estado;
Modelo modelo;

using namespace std;

void initEstado() {
	estado.delay = 10;
	estado.doubleBuffer = true;
}

void initModelo() {
	modelo.anim = 1;
	modelo.anguloInicial = 0.0;
	modelo.raio = RAIO_INI + 0.01;
	modelo.angulo = 0.0;
	modelo.inc = 0.01;
}

void pentagrama(GLdouble xCentro, GLdouble yCentro, GLdouble raio) {
	GLint i, n = 5;
	GLdouble ang = 2.0 * M_PI / (GLdouble)n;

	glBegin(GL_LINE_LOOP);

	glColor3f(1.0, 1.0, 1.0);

	for (i = 0; i < n; i++) {
		glVertex2d(xCentro + raio * cos(2.0 * i * ang),
			yCentro + raio * sin(2.0 * i * ang));
	}

	glEnd();
}

void desenhaCena() {
	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix();
		glRotated(modelo.angulo, 0.0, 0.0, 1.0);
		pentagrama(0.0, 0.0, modelo.raio);
	glPopMatrix();
	glFlush();

	if (estado.doubleBuffer) {
		glutSwapBuffers();
	}
}

void init(void)
{
	// cor do fundo
	glClearColor(0.63, 0.21, 0.66, 0.0);

	// inicializar o valores de visualiza��o
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	//glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);

	gluOrtho2D(-1, 1, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluOrtho2D(0, glutGet(GLUT_INIT_WINDOW_WIDTH), 0, glutGet(GLUT_INIT_WINDOW_HEIGHT));
}


// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	GLint size;

	if (width < height)
		size = width;
	else
		size = height;

	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)size, (GLint)size);


	// Matriz Projec��o
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projec��o ortogonal 2D, com profundidade (Z) entre -1 e 1
	gluOrtho2D(-1, 1, -1, 1);

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformac�es dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Timer(int value) {
	glutTimerFunc(estado.delay, Timer, 0);
	switch (modelo.anim) {
		case 1: 
		case 3:
			modelo.raio += modelo.inc;
			if (modelo.raio <= RAIO_INI || modelo.raio >= RAIO_INI + 0.5) {
				modelo.inc *= -1.0;
				modelo.anim++;
			}
		case 2:
		case 4:
			modelo.angulo++;
			if (modelo.angulo >= modelo.anguloInicial + 90.0) {
				modelo.anguloInicial = modelo.angulo;
				if (modelo.anim < 4) {
					modelo.anim++;
				}
				else {
					modelo.anim = 1;
				}
			}
		break;
	}
	cout << "inc: " << modelo.inc << endl;
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB);

	glutInitWindowSize(500, 500);

	glutInitWindowPosition(100, 100);

	glutTimerFunc(estado.delay, Timer, 0);

	glutCreateWindow("pentagrama");

	init();
	initEstado();
	initModelo();

	glutReshapeFunc(Reshape);
	glutDisplayFunc(desenhaCena);

	glutMainLoop();

	return 0;
}